package tn.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Device
 *
 */
@Entity

public class Device implements Serializable {

	@Id
	private String uniqueidentifier;
	@Enumerated(EnumType.STRING)
	private DeviceType deviceType;
	private String modelName;
	@Enumerated(EnumType.STRING)
	private Brand brand;
	@ManyToOne
	private Employee employee;
	private static final long serialVersionUID = 1L;

	
	public String getUniqueidentifier() {
		return uniqueidentifier;
	}


	public void setUniqueidentifier(String uniqueidentifier) {
		this.uniqueidentifier = uniqueidentifier;
	}


	public DeviceType getDeviceType() {
		return deviceType;
	}


	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}


	public String getModelName() {
		return modelName;
	}


	public void setModelName(String modelName) {
		this.modelName = modelName;
	}


	public Brand getBrand() {
		return brand;
	}


	public void setBrand(Brand brand) {
		this.brand = brand;
	}


	public Employee getEmployee() {
		return employee;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	public Device() {
		super();
	}
   
}
