package tn.esprit.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Employee
 *
 */
@Entity

public class Employee implements Serializable {

	@Id // PrimaryKey
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long matricule;
	private String firstName;
	private String lastName;
	private String password;
	@Enumerated(EnumType.STRING)
	private EmployeeType employeetype;
	@OneToMany(mappedBy="employee",fetch=FetchType.EAGER,cascade=CascadeType.REMOVE)
	private List<Device> lstdevice;
	private static final long serialVersionUID = 1L;

	
	public long getMatricule() {
		return matricule;
	}


	public void setMatricule(long matricule) {
		this.matricule = matricule;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public EmployeeType getEmployeetype() {
		return employeetype;
	}


	public void setEmployeetype(EmployeeType employeetype) {
		this.employeetype = employeetype;
	}


	public List<Device> getLstdevice() {
		return lstdevice;
	}


	public void setLstdevice(List<Device> lstdevice) {
		this.lstdevice = lstdevice;
	}


	public Employee() {
		super();
	}

}
