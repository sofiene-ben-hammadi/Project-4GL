package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.entities.Device;

@Local
public interface DeviceServiceLocal {

	public void addDevice(Device e);
	public void updateDevice(Device e);
	public void deleteDevice(String id);
	public Device getDevicebyUid(String id);
	public List<Device> getAllDevice();
	public void affecterDeviceEmployee(String deviceUniqueid,long matricule);
	public List<Device> getDevicesByEmployee(long matricule);
}
