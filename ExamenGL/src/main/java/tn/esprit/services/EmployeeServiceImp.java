package tn.esprit.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Employee;

/**
 * Session Bean implementation class EmployeeServiceImp
 */
@Stateless

public class EmployeeServiceImp implements EmployeeServiceRemote,EmployeeServiceLocal {

	@PersistenceContext
	EntityManager em;
    /**
     * Default constructor. 
     */
    public EmployeeServiceImp() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addEmp(Employee e) {
		// TODO Auto-generated method stub
		em.persist(e);
		
	}

	@Override
	public void updateEmp(Employee e) {
		// TODO Auto-generated method stub
		em.merge(e);
	}

	@Override
	public void deleteEmp(long id) {
		// TODO Auto-generated method stub
		em.remove(getEmpByMatricule(id));
	}

	@Override
	public Employee getEmpByMatricule(long id) {
		// TODO Auto-generated method stub
		return em.find(Employee.class, id);
	}

	@Override
	public List<Employee> getAllEmp() {
		// TODO Auto-generated method stub
		Query q = em.createQuery("select e from Employee e",Employee.class);
		return q.getResultList();
	}

	@Override
	public Employee authentification(long matricule, String pwd) {
		// TODO Auto-generated method stub
		Employee e;
		Query req = em.createQuery("select e from Employee e where e.matricule = :mat and e.password like :password");
//		TypedQuery<Employee> req = em.createQuery("select e from Employee e where e.matricule = :mat and e.password like :password",Employee.class);

		req.setParameter("mat", matricule).setParameter("password", pwd);
		try {
			e = (Employee) req.getSingleResult();
			return e;
		} catch (Exception e2) {
			// TODO: handle exception
			System.out.println("Employee not found");
		}
		
		return null;
	}

}
