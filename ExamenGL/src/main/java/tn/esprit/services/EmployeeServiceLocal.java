package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.entities.Employee;

@Local
public interface EmployeeServiceLocal {

	public void addEmp(Employee e);
	public void updateEmp(Employee e);
	public void deleteEmp(long id);
	public Employee authentification(long matricule, String pwd);
	public Employee getEmpByMatricule(long id);
	public List<Employee> getAllEmp();
}
