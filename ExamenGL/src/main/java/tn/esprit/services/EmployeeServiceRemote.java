package tn.esprit.services;

import java.util.List;

import javax.ejb.Remote;


import tn.esprit.entities.Employee;

@Remote
public interface EmployeeServiceRemote {

	public void addEmp(Employee e);
	public void updateEmp(Employee e);
	public void deleteEmp(long id);
	public Employee getEmpByMatricule(long id);
	public List<Employee> getAllEmp();

	
	
}
