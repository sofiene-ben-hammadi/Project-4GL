package tn.esprit.beans;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.entities.Employee;
import tn.esprit.entities.EmployeeType;
import tn.esprit.services.EmployeeServiceLocal;

@ManagedBean
@SessionScoped
public class LoginBean {

	@EJB
	EmployeeServiceLocal empservice;

	private Employee employe;

	private String login;
	private String password;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String authentification() {
		employe = empservice.authentification(Long.valueOf(login), password);
		if (employe != null && employe.getEmployeetype().equals(EmployeeType.ADMIN)) {
			return "/admin/affecter?faces-redirect=true";
		} else if (employe != null && employe.getEmployeetype().equals(EmployeeType.EMP)) {
			return "/emp/afficher?faces-redirect=true";
		} else {
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("Employee inexistant"));
		}
		return null;

	}

	public String logOut() {

		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/index?faces-redirect=true";

	}

	public Employee getEmploye() {
		return employe;
	}

	public void setEmploye(Employee employe) {
		this.employe = employe;
	}

}
