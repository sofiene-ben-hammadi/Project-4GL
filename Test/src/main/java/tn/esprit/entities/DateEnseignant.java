package tn.esprit.entities;

import java.io.Serializable;
import java.lang.Long;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: DateEnseignant
 *
 */
@Entity

@IdClass(DateEnseignantPK.class)
public class DateEnseignant implements Serializable {

	   
	@Id
	private long idEnseignant;   
	@Id
	private Long idEco;
	@Temporal(TemporalType.DATE)
	private Date date ;
	@ManyToOne
	@JoinColumn(name="idEco",referencedColumnName="idEcole",insertable=false,updatable=false)
	private Ecole ecole ;
	@ManyToOne
	@JoinColumn(name="idEnseignant",referencedColumnName="idEns",insertable=false,updatable=false)
	Enseignant ens ;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	private static final long serialVersionUID = 1L;

	public DateEnseignant() {
		super();
	}   
	public long getIdEnseignant() {
		return this.idEnseignant;
	}

	public void setIdEnseignant(long idEnseignant) {
		this.idEnseignant = idEnseignant;
	}
	public Long getIdEco() {
		return idEco;
	}
	public void setIdEco(Long idEco) {
		this.idEco = idEco;
	}
	public Ecole getEcole() {
		return ecole;
	}
	public void setEcole(Ecole ecole) {
		this.ecole = ecole;
	}
	public Enseignant getEns() {
		return ens;
	}
	public void setEns(Enseignant ens) {
		this.ens = ens;
	}   
	
}
