package tn.esprit.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;

/**
 * Entity implementation class for Entity: Ecole
 *
 */
@Entity

public class Ecole implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long idEcole;
	public List<DateEnseignant> getListEnseignat() {
		return listEnseignat;
	}
	public void setListEnseignat(List<DateEnseignant> listEnseignat) {
		this.listEnseignat = listEnseignat;
	}
	public List<Salle> getListSalle() {
		return listSalle;
	}
	public void setListSalle(List<Salle> listSalle) {
		this.listSalle = listSalle;
	}

	private String name;
	@OneToMany(mappedBy="ecole")
	private List<DateEnseignant> listEnseignat ;
	@OneToMany(cascade=CascadeType.REMOVE,fetch=FetchType.EAGER)
	private List<Salle> listSalle ;

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	private static final long serialVersionUID = 1L;

	public Ecole() {
		super();
	}   
	public long getIdEcole() {
		return this.idEcole;
	}

	public void setIdEcole(long idEcole) {
		this.idEcole = idEcole;
	}
   
}
