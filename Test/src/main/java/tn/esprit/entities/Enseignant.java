package tn.esprit.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Enseignant
 *
 */
@Entity

public class Enseignant implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long idEns;
	private String name ;
	private String mail;
	private Date dateNaissance;
	
	@Enumerated(EnumType.STRING)
	private TypeEns type;
	@OneToMany(mappedBy="enseignant")
	private List<Cours> cours ;

	public List<Cours> getCours() {
		return cours;
	}
	public void setCours(List<Cours> cours) {
		this.cours = cours;
	}
	public List<DateEnseignant> getDateEenseignat() {
		return dateEenseignat;
	}
	public void setDateEenseignat(List<DateEnseignant> dateEenseignat) {
		this.dateEenseignat = dateEenseignat;
	}
	public Salle getSalle() {
		return salle;
	}
	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	@OneToMany(mappedBy="ens")
	private List<DateEnseignant> dateEenseignat;
	@OneToOne(mappedBy="enseignant")
	private Salle salle;
	public TypeEns getType() {
		return type;
	}
	public void setType(TypeEns type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	private static final long serialVersionUID = 1L;

	public Enseignant() {
		super();
	}   
	public long getIdEns() {
		return this.idEns;
	}

	public void setIdEns(long idEns) {
		this.idEns = idEns;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public Date getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
   
}
