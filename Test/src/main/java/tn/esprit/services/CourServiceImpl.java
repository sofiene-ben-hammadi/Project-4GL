package tn.esprit.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Cours;

/**
 * Session Bean implementation class CourServiceImpl
 */
@Stateless
public class CourServiceImpl implements CourServiceLocal, CourServiceRemote {

	@PersistenceContext
	EntityManager em;
	
    /**
     * Default constructor. 
     */
    public CourServiceImpl() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addCour(Cours c) {
		// TODO Auto-generated method stub
		em.persist(c);
	}

	@Override
	public Cours updateCour(Cours c) {
		// TODO Auto-generated method stub
		return em.merge(c);
	}

	@Override
	public Cours findCourById(long id) {
		// TODO Auto-generated method stub
		return em.find(Cours.class, id);
	}

	@Override
	public Cours findCourByName(String name) {
		// TODO Auto-generated method stub
		TypedQuery<Cours> query = em.createQuery
				("select c from Cours c where c.nom like :n",Cours.class);
				query.setParameter("n", name);
		return query.getSingleResult();
	}

	@Override
	public void deleteCour(long id) {
		// TODO Auto-generated method stub
		em.remove(findCourById(id));
	}

	@Override
	public List<Cours> getAllCour() {
		// TODO Auto-generated method stub
		Query query = em.createQuery("select c from Cours c");
		return query.getResultList();
	}

}
