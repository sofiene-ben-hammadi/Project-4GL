package tn.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.entities.Ecole;

@Remote
public interface EcoleServiceRemote {
	public void addEcole(Ecole e);

	public Ecole updateEcole(Ecole e);

	public Ecole findEcoleById(long id);

	public Ecole findEcoleByName(String name);

	public void deleteEcole(long id);

	public List<Ecole> getAllEcole();

}
