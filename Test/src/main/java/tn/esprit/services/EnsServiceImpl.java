package tn.esprit.services;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Enseignant;

/**
 * Session Bean implementation class EnsServiceImpl
 */
@Stateless
public class EnsServiceImpl implements EnsServiceLocal, EnsServiceRemote {

	@PersistenceContext
	EntityManager em;
    /**
     * Default constructor. 
     */
    public EnsServiceImpl() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void deleteEns(long id) {
        // TODO Auto-generated method stub
    	em.remove(findEnsById(id));
    }

    @Override
    public Enseignant updateEns(Enseignant c) {
        // TODO Auto-generated method stub
			return em.merge(c);
    }

    @Override
    public Enseignant findEnsById(long id) {
        // TODO Auto-generated method stub
			return em.find(Enseignant.class, id);
    }
    
    @Override
    public void addEns(Enseignant c) {
        // TODO Auto-generated method stub
    	em.persist(c);
    }

    @Override
    public Enseignant findEnsByName(String name) {
        // TODO Auto-generated method stub
    	TypedQuery<Enseignant> req = em.createQuery("select e from Enseignant e where e.name like :nom",Enseignant.class);
    	req.setParameter("nom", name);
			return req.getSingleResult();
    }

    @Override
    public List<Enseignant> getAllEns() {
        // TODO Auto-generated method stub
    	TypedQuery<Enseignant> req = em.createQuery
    			("select e from Enseignant e",Enseignant.class);
			return req.getResultList();
    }

	@Override
	public Enseignant authentifier(long login, String name) 
	
	{
		Enseignant ens =  null;
		
		TypedQuery<Enseignant> req = em.createQuery("select e from Enseignant e where e.idEns = :log and e.name like :pwd",Enseignant.class);
		req.setParameter("log", login).setParameter("pwd", name);
		
		try {
			ens  = req.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Enseignant inexistant");
		}
		
		return ens;
	}

}
