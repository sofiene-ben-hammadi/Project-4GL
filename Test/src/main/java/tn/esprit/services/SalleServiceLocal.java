package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.entities.Salle;

@Local
public interface SalleServiceLocal {
	public void addSalle(Salle s);

	public Salle updateSalle(Salle s);

	public Salle findSalleById(long id);

	public Salle findSalleByName(String name);

	public void deleteSalle(long id);

	public List<Salle> getAllSalle();

}
