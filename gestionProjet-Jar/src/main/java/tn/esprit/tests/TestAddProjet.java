package tn.esprit.tests;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Langage;
import tn.esprit.entities.Projet;
import tn.esprit.services.ServiceProjetRemote;
import tn.esprit.services.ServiceUserRemote;

public class TestAddProjet {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub

		InitialContext context = new InitialContext();
		ServiceProjetRemote sp = (ServiceProjetRemote) context
				.lookup("gestionProjet/ServiceProjet!tn.esprit.services.ServiceProjetRemote");
		Projet p = new Projet();
		Projet p2 = new Projet();
		p.setTitle("Sys de pointage");
		p.setLangagedev(Langage.dotnet);
		p.setDescription("Outils de pointage");
		p.setNbrOfClass(32);
		p.setDocumentation(false);
		//----------//
		p2.setTitle("Gestion RH");
		p2.setLangagedev(Langage.php);
		p2.setDescription("Outils de gestion des ressources humaines");
		p2.setNbrOfClass(78);
		p2.setDocumentation(true);
		
		sp.addProjet(p);
		sp.addProjet(p2);
		
	}

}
