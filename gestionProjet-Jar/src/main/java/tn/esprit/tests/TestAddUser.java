package tn.esprit.tests;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.User;
import tn.esprit.services.ServiceUserRemote;

public class TestAddUser {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub

		InitialContext context = new InitialContext();
		ServiceUserRemote su = (ServiceUserRemote) context
				.lookup("gestionProjet/ServiceUser!tn.esprit.services.ServiceUserRemote");
		User u = new User();
		u.setName("Mohamed");
		u.setLogin("examen");
		u.setPassword("esprit");
		u.setEmail("examen@esprit.tn");
		su.addUser(u);
	}

}
