package tn.esprit.beans;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.entities.User;
import tn.esprit.services.ServiceUserLocal;

@ManagedBean
@SessionScoped
public class LoginBean {

	@EJB
	ServiceUserLocal su;

	private String login;
	private String password;
	private User u;

	public User getU() {
		return u;
	}

	public void setU(User u) {
		this.u = u;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String authentifier() {
		u = su.authentification(login, password);
		if (u != null) {
			return "/user/acceuil?faces-redirect=true";
		} else {
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("User inexistant"));
		}
		return null;

	}

}
