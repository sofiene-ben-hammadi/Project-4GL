package tn.esprit.beans;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.entities.Langage;
import tn.esprit.entities.Projet;
import tn.esprit.services.ServiceProjetLocal;

@ManagedBean
@ViewScoped
public class ProjetBean {

	@EJB
	ServiceProjetLocal sp;
	private Projet p;
	private Langage[] langues;
	
	public Langage[] getLangues() {
		return Langage.values();
	}
	public void setLangues(Langage[] langues) {
		this.langues = langues;
	}
	public Projet getP() {
		return p;
	}
	public void setP(Projet p) {
		this.p = p;
	}
	@PostConstruct
	public void init(){
		getLangues();
		p = new Projet();
	}
	public void ajouter(){
		sp.addProjet(p);
		p = new Projet();
	}
	
}
