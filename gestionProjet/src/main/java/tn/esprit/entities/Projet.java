package tn.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Projet
 *
 */
@Entity

public class Projet implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idProjet;
	private String title;
	private String description;
	private int nbrOfClass;
	@Enumerated(EnumType.STRING)
	private Langage langagedev;
	private boolean documentation;
	@ManyToOne
	private User user;

	public int getIdProjet() {
		return idProjet;
	}

	public void setIdProjet(int idProjet) {
		this.idProjet = idProjet;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getNbrOfClass() {
		return nbrOfClass;
	}

	public void setNbrOfClass(int nbrOfClass) {
		this.nbrOfClass = nbrOfClass;
	}

	public Langage getLangagedev() {
		return langagedev;
	}

	public void setLangagedev(Langage langagedev) {
		this.langagedev = langagedev;
	}

	public boolean isDocumentation() {
		return documentation;
	}

	public void setDocumentation(boolean documentation) {
		this.documentation = documentation;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	private static final long serialVersionUID = 1L;

	public Projet() {
		super();
	}

}
