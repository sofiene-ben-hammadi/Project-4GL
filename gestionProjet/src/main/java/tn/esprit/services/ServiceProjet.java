package tn.esprit.services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.entities.Projet;

/**
 * Session Bean implementation class ServiceProjet
 */
@Stateless

public class ServiceProjet implements ServiceProjetRemote, ServiceProjetLocal {

	@PersistenceContext
	EntityManager em;
    /**
     * Default constructor. 
     */
    public ServiceProjet() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addProjet(Projet p) {
		// TODO Auto-generated method stub
		em.persist(p);
	}

}
