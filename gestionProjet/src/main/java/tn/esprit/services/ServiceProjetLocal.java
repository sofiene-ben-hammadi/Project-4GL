package tn.esprit.services;

import javax.ejb.Local;

import tn.esprit.entities.Projet;

@Local
public interface ServiceProjetLocal {

	void addProjet(Projet p);

}
