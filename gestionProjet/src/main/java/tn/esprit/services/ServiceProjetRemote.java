package tn.esprit.services;

import javax.ejb.Remote;

import tn.esprit.entities.Projet;

@Remote
public interface ServiceProjetRemote {

	void addProjet(Projet p);
}
