package tn.esprit.services;

import javax.ejb.Local;

import tn.esprit.entities.User;

@Local
public interface ServiceUserLocal {

	void addUser(User u);

	User authentification(String login, String pwd);
}
