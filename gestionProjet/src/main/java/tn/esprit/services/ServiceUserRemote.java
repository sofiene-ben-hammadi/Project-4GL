package tn.esprit.services;

import javax.ejb.Remote;

import tn.esprit.entities.User;

@Remote
public interface ServiceUserRemote {

	void addUser(User u);

	User authentification(String login, String pwd);

}
