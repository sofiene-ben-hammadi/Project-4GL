package tn.esprit.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Departement
 *
 */
@Entity

public class Departement implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idDepartement;
	private String nomDepartement;
	@OneToMany
	private List<Employer> lstemployee;
	private static final long serialVersionUID = 1L;

	public Departement() {
		super();
	}

	public int getIdDepartement() {
		return idDepartement;
	}

	public void setIdDepartement(int idDepartement) {
		this.idDepartement = idDepartement;
	}

	public String getNomDepartement() {
		return nomDepartement;
	}

	public void setNomDepartement(String nomDepartement) {
		this.nomDepartement = nomDepartement;
	}

	public List<Employer> getLstemployee() {
		return lstemployee;
	}

	public void setLstemployee(List<Employer> lstemployee) {
		this.lstemployee = lstemployee;
	}

}
