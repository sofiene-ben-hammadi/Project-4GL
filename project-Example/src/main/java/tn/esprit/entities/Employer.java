package tn.esprit.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Entity implementation class for Entity: Employer
 *
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "t_employer")
@DiscriminatorColumn(name = "type_Employer")
public class Employer implements Serializable {

	@Id
	private Integer id;
	private static final long serialVersionUID = 1L;

	public Employer() {
		super();
	}

	@Embedded
	private Adresse adresse;
	@ManyToOne
	private Entreprise entreprise;
	@OneToMany(mappedBy="employee")
	private List<Tache> lsttaches;

	public List<Tache> getLsttaches() {
		return lsttaches;
	}

	public void setLsttaches(List<Tache> lsttaches) {
		this.lsttaches = lsttaches;
	}

	public Entreprise getEntreprise() {
		return entreprise;
	}

	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	@Enumerated(EnumType.STRING)
	private Nationalite nationalite;

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Nationalite getNationalite() {
		return nationalite;
	}

	public void setNationalite(Nationalite nationalite) {
		this.nationalite = nationalite;
	}

	@Column(name = "c_name")
	private String name;

	@NotNull
	private String lastName;

}
