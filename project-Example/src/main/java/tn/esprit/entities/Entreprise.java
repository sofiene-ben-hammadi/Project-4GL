package tn.esprit.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Entreprise
 *
 */
@Entity

public class Entreprise implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idEntreprise;
	private String nomEntreprise;
	@OneToMany(mappedBy="entreprise")
	private List<Employer> lstemp;

	public List<Employer> getLstemp() {
		return lstemp;
	}

	public void setLstemp(List<Employer> lstemp) {
		this.lstemp = lstemp;
	}

	public int getIdEntreprise() {
		return idEntreprise;
	}

	public void setIdEntreprise(int idEntreprise) {
		this.idEntreprise = idEntreprise;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	private static final long serialVersionUID = 1L;

	public Entreprise() {
		super();
	}

}
