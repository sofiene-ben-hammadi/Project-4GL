package tn.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Ingenieur
 *
 */
@Entity
@DiscriminatorValue(value="Ing")
public class Ingenieur extends Employer implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Ingenieur() {
		super();
	}
   
	public String getDepartement() {
		return departement;
	}

	public void setDepartement(String departement) {
		this.departement = departement;
	}

	private String departement;
}
