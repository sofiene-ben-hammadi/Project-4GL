package tn.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Technicien
 *
 */
@Entity
@DiscriminatorValue(value="Tech")
public class Technicien extends Employer implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Technicien() {
		super();
	}
	
	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	private String grade;
   
}
