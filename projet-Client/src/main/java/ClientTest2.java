import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Ecole;
import tn.esprit.entities.Salle;
import tn.esprit.services.EcoleServiceRemote;
import tn.esprit.services.SalleServiceRemote;

public class ClientTest2 {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		InitialContext context = new InitialContext();
		SalleServiceRemote salleservice = (SalleServiceRemote) context.lookup
				("TP/ServiceSalleImpl!tn.esprit.services.SalleServiceRemote");

		EcoleServiceRemote es = (EcoleServiceRemote) context.lookup
				("TP/EcoleServiceImpl!tn.esprit.services.EcoleServiceRemote");
		
		
		Salle sale = new Salle();
		sale.setNom("C3");
		
		Salle sale1 = new Salle();
		sale1.setNom("C388");
		
		Salle sale2 = new Salle();
		sale2.setNom("C387");
		
	//	salleservice.addSalle(sale);
		
	//	Salle sale = salleservice.findSalleById(5);
		List<Salle> lsal = new ArrayList<>();
		lsal.add(sale);
		lsal.add(sale1);
		lsal.add(sale2);
		
		Ecole e = new Ecole();
		e.setName("Ecole10");
		
		e.setListSalle(lsal);
		
		es.addEcole(e);
		
		List<Ecole> lecoles = es.getAllEcole();
		
		for (Ecole ecole2 : lecoles) {
			System.out.println(ecole2.getName());
		}

		Ecole ec = es.findEcoleById(10);
		
		List<Salle> ls = ec.getListSalle();
		
		for (Salle salle : ls) {
			System.out.println(salle.getNom());
		}

		//es.deleteEcole(8);
	}

}
