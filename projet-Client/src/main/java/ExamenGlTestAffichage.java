import java.net.NetworkInterface;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Device;
import tn.esprit.entities.Employee;
import tn.esprit.entities.EmployeeType;
import tn.esprit.services.DeviceServiceRemote;
import tn.esprit.services.EmployeeServiceRemote;

public class ExamenGlTestAffichage {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		InitialContext context = new InitialContext();
		DeviceServiceRemote ds = (DeviceServiceRemote) context
				.lookup("ExamenGL/DeviceServiceImpl!tn.esprit.services.DeviceServiceRemote");

		List<Device> lst = ds.getDevicesByEmployee(3);
		for (Device device : lst) {
			System.out.println(device.getBrand());
			System.out.println(device.getModelName());
			System.out.println(device.getUniqueidentifier());
			System.out.println(device.getDeviceType());
		}

	}

}
