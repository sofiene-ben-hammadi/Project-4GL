package tn.esprit.beans;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.entities.Enseignant;
import tn.esprit.services.EnsServiceLocal;

@ManagedBean
@SessionScoped
public class LoginBean {

	@EJB
	EnsServiceLocal ensservice;
	
	
	private Enseignant ens;

	private String login;
	private String password;
	
	
	public Enseignant getEns() {
		return ens;
	}

	public void setEns(Enseignant ens) {
		this.ens = ens;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public String authentification()
	{
		ens = ensservice.authentifier(Long.valueOf(login), password);
		if (ens != null) {
			return "/enseignant/welcome?faces-redirect=true";
		}
		else {
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("Enseignant inexistant"));
		}
		return null;
		
	}
	
	public String logOut(){
		
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/index?faces-redirect=true";
		
		
	}
	
}
