package tn.esprit.test;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.traducteur.TraducteurRemote;

public class TestTraducteur {

	public static void main(String[] args) throws NamingException{
	
		InitialContext context = new InitialContext();
		
		TraducteurRemote traducteurRemote = (TraducteurRemote) 
				context.lookup("traducteurGL/TraducteurRemoteImpl!tn.esprit.traducteur.TraducteurRemote");
	
		System.out.println(traducteurRemote.traduire("Bonjour"));
		System.out.println(traducteurRemote.traduire("Hello"));
		System.out.println(traducteurRemote.traduire("gggg"));
	}
	
	
}
